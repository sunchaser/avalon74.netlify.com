import gulp from 'gulp';
import gulpSass from 'gulp-sass';
import dartSass from 'sass';

const sass = gulpSass(dartSass);

export function lightbox() {
    return gulp.src([
        'node_modules/lightbox2/dist/**',
    ])
        .pipe(gulp.dest('assets/lightbox'))
}

export function main_style() {
    return gulp.src([
        '_ui/stylesheets/style-r2.scss'
    ])
        .pipe(sass())
        .pipe(gulp.dest('css'));
}

export function uralfest_style() {
    return gulp.src([
        '_ui/stylesheets/uralfest.scss'
    ])
        .pipe(sass())
        .pipe(gulp.dest('uralfest/css'));
}

export const build = gulp.parallel(lightbox, main_style, uralfest_style);
export default build;

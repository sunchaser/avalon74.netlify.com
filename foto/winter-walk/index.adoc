---
title: Зимняя прогулка
layout: gallery
image: /foto/winter-walk/my-s-konyom-i-pejzazh.jpg
gallery:
  path: /foto/winter-walk
  images:
  - file:  posle-trenirovki-2.jpg
    title: После тренировки 2
  - file:  posle-trenirovki.jpg
    title: После тренировки
  - file:  na-skale-3.jpg
    title: На скале 3
  - file:  na-skale-2.jpg
    title: На скале 2
  - file:  na-skale.jpg
    title: На скале
  - file:  na-rycarskoj-pustoshi.jpg
    title: На Рыцарской пустоши
  - file:  na-ldu-3.jpg
    title: На льду 3
  - file:  na-ldu-2.jpg
    title: На льду 2
  - file:  na-ldu.jpg
    title: На льду
  - file:  na-beregu.jpg
    title: На берегу
  - file:  my-s-konyom-i-pejzazh.jpg
    title: Мы с конём и пейзаж
  - file:  galopom-po-ldu.jpg
    title: Галопом по льду
  - file:  vdol-berega.jpg
    title: Вдоль берега
---

Зимняя прогулка

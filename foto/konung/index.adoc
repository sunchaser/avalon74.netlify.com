---
title: Отъезд конунга
layout: gallery
image: /foto/konung/bay.jpg
gallery:
  path: /foto/konung
  images:
  - file:  bay.jpg
    title: Прощание с Агдиром
  - file:  bors.jpg
    title: В дальний путь
  - file:  bow.jpg
    title: Халли и Хильда
  - file:  faust.jpg
    title: Перед сьёмкой
  - file:  go-go.jpg
    title: По коням
  - file:  konung.jpg
    title: Конунг и его люди
  - file:  people.jpg
    title: Провожающие
  - file:  riders.jpg
    title: Всадники
  - file:  shields.jpg
    title: Чей пряник лучше?
---

Отъезд конунга

''''

Фотографии со сьёмок сцены отьезда конунга из Агдира
